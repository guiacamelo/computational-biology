## Read 3 sequences and compare with Homo Sapiens sequence
## Find possible alignments
## implement dot matrix for the alignment
## print alignment score
## State wich one of the sequences have the bigger alignment

import time
import numpy
start_time = time.time()
fHS = open("sequences/AAA631878.1", "r") ## HomoSapiens
fMM = open("sequences/AAA37807.1", "r") ## MusMusculus
fRN = open("sequences/AAA41305.1", "r") ## RattusNorvegicus

headerHS = fHS.readline()
headerMM = fMM.readline()
headerRM = fRN.readline()

print headerHS
print headerMM
print headerRM

strHS = fHS.read();
strMM = fMM.read();
strRM = fRN.read();

fHS.close()
fMM.close()
fRN.close()

strHS = strHS.replace('\n','').replace('\r','')
strMM = strMM.replace('\n','').replace('\r','') 
strRM = strRM.replace('\n','').replace('\r','') 



matrixHSxMM = [[0 for x in range(len(strHS))] for x in range(len(strMM))] 
matrixHSxRM = [[0 for x in range(len(strHS))] for x in range(len(strRM))] 
numrowsHSvsMM = len(matrixHSxMM)  
numcolsHSvsMM = len(matrixHSxMM[0])
numrowsHSvsRM = len(matrixHSxRM)  
numcolsHSvsRM = len(matrixHSxRM[0]) 
print "Number of Rows"
print numcolsHSvsMM ,numrowsHSvsMM 
print numcolsHSvsRM ,numrowsHSvsRM

#

print "Iterating Homo Sapiens And Mus Musculus"
aHSxMM =numpy.array(matrixHSxMM)
aHSxRM =numpy.array(matrixHSxRM)
d = 0
for (x,y), value in numpy.ndenumerate(aHSxMM):
	if (strHS[y] == strMM[x]):
		aHSxMM[x][y] = 1
		
for (z,w), value in numpy.ndenumerate(aHSxRM):
	if (strHS[w] == strRM[z]):
		aHSxRM[z][w] = 1
print "Similarity between Homo Sapiens And Mus Musculus"	
print sum(aHSxMM.diagonal())
print "Similarity between Homo Sapiens And Rattus Norvegicus"	
print sum(aHSxRM.diagonal())
#print aHSxMM[0][218]
#print len(aHSxMM)
#print matrixHSxRM.size()
print("--- %s seconds ---" % (time.time() - start_time))
