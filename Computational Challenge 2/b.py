def needlemanWunshFunction(StringA,StringB):
	#needleman-Wunsh Variables
	gap = -4
	match = 5
	mismach = -3
	alignmentA = ""
	alignmentB = ""
	similarityAlignment = 0.0
	n_maches = 0
	bMatrix = [[0 for x in range(len(StringA))] for x in range(len(StringB))] 
	Matrix =numpy.array(bMatrix)
	#needleman-Wunsh Functions
	def initialization(aMatrix):
		for i in range(len(aMatrix)):
			aMatrix[i][0] = gap * i
		for j in range(len(aMatrix[0])):
			aMatrix[0][j] = gap * j 
		return aMatrix
	def scoring(aMatrix,strA,strB):
		for i in range(len(aMatrix))[1:]:
			for j in range(len(aMatrix[0]))[1:]:
				if strA[j]==strB[i]:
					m = match
				else:
					m = mismach
				local_match = aMatrix[i-1][j-1] + m
				local_delet = aMatrix[i-1][j] + gap
				local_insert = aMatrix[i][-1] + gap
				aMatrix[i][j] = max (local_match,local_delet,local_insert)
		return aMatrix
	def traceback(aMatrix,strA,strB):
		alA = ""
		alB = ""
		i = len(aMatrix)-1
		j = len(aMatrix[0])-1
		while(i>0 and j>0):
			score =	 aMatrix[i][j]
			scoreDiagonal = aMatrix[i-1][j-1]
			scoreAbove = aMatrix[i][j-1]
			scoreBeside = aMatrix[i-1][j]
			if strA[j]==strB[i]:
				m = match
			else:
				m = mismach
			if (score == scoreDiagonal + m):
				alA = strA[j] + alA
				alB = strB[i] + alB
				i-=1
				j-=1
			elif (Score == scoreBeside + d):
				alA = strA[j] + alA
				alB = "-" + alB
				j-=1
			elif (Score == scoreAbove + d):
				alA = "-" + alA
				alB = strB[i] + alB
				i-=1
			else:
				print "NOT GOOD!!!"
		while (i>0):
			alA = "-" + alA
			alB = strB[i] + alB
			i-=1
		while (j>0):
			alA = strA[j] + alA
			alB = "-" + alB
			j-=1

		return (alA, alB)
	def similarity(alA, alB):
		lenAB = max(len(alA),len(alB))
		similarityValue = 0.0
		n_maches =0	 
		c = 0
		scoreAlinhamento = 0
		while (c<lenAB):
			if(((alA[c]=='-')and(alB[c]!='-'))or((alA[c]!='-')and(alB[c]=='-'))):
				scoreAlinhamento-=4
			elif (alA[c]==alB[c])and(alB[c]!='-'):
				n_maches+=1
				scoreAlinhamento+=5
			elif (alA[c]!=alB[c]):
				scoreAlinhamento-=3
			c+=1
		similarityValue = ((n_maches*1.0)/(lenAB*1.0))*100
		return (similarityValue, n_maches,scoreAlinhamento)

	numpy.set_printoptions(threshold='nan')
	Matrix = initialization(Matrix)
	Matrix = scoring(Matrix, StringA,StringB)
	#print Matrix	
	alignmentA, alignmentB = traceback(Matrix, StringA,StringB)
	similarityAlignment,n_maches,scoreAlinhamento = similarity(alignmentA,alignmentB)
	print "AlignmentA is " , alignmentA 
	print "AlignmentB is " , alignmentB
	print "Similarity Alignment is " , similarityAlignment, " The alignments had " , n_maches," matches. And the alignmet score was ", scoreAlinhamento

import time
import numpy
start_time = time.time()
fHS = open("sequences/NP005359.1", "r") ## HomoSapiens
fMM = open("sequences/NP038621.2", "r") ## MusMusculus
fRN = open("sequences/AAH70511.1", "r") ## RattusNorvegicus
fLE = open("sequences/P02168", "r") ## Lemur
fWA = open("sequences/1CIO", "r") ## Whale

headerHS = fHS.readline()
headerMM = fMM.readline()
headerRM = fRN.readline()
headerLE = fLE.readline()
headerWA = fWA.readline()


print headerHS
print headerMM
print headerRM
print headerLE
print headerWA
strHS = fHS.read();
strMM = fMM.read();
strRM = fRN.read();
strLE = fLE.read();
strWA = fWA.read();
fHS.close()
fMM.close()
fRN.close()
fLE.close()
fWA.close()
strHS = strHS.replace('\n','').replace('\r','')
strMM = strMM.replace('\n','').replace('\r','') 
strRM = strRM.replace('\n','').replace('\r','') 
strLE = strLE.replace('\n','').replace('\r','') 
strWA = strWA.replace('\n','').replace('\r','') 

print "Similarity between Homo Sapiens And MusMusculus"
needlemanWunshFunction(strHS,strMM)

print "Similarity between Homo Sapiens And RattusNorvegicus"
needlemanWunshFunction(strHS,strRM)

print "Similarity between Homo Sapiens And Lemur"
needlemanWunshFunction(strHS,strLE)

print "Similarity between Homo Sapiens And Whale"
needlemanWunshFunction(strHS,strWA)

print("--- %s seconds ---" % (time.time() - start_time))
