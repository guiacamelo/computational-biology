## Read 3 sequences and compare with Homo Sapiens sequence
## Find possible alignments
## implement dot matrix for the alignment
## print alignment score
## State wich one of the sequences have the bigger alignment

import time
import numpy
start_time = time.time()
fHS = open("sequences/NP005359.1", "r") ## HomoSapiens
fMM = open("sequences/NP038621.2", "r") ## MusMusculus
fRN = open("sequences/AAH70511.1", "r") ## RattusNorvegicus
fLE = open("sequences/P02168", "r") ## Lemur
fWA = open("sequences/1CIO", "r") ## Whale

headerHS = fHS.readline()
headerMM = fMM.readline()
headerRM = fRN.readline()
headerLE = fLE.readline()
headerWA = fWA.readline()


print headerHS
print headerMM
print headerRM
print headerLE
print headerWA

strHS = fHS.read();
strMM = fMM.read();
strRM = fRN.read();
strLE = fLE.read();
strWA = fWA.read();

fHS.close()
fMM.close()
fRN.close()
fLE.close()
fWA.close()

strHS = strHS.replace('\n','').replace('\r','')
strMM = strMM.replace('\n','').replace('\r','') 
strRM = strRM.replace('\n','').replace('\r','') 
strLE = strLE.replace('\n','').replace('\r','') 
strWA = strWA.replace('\n','').replace('\r','') 

		
matrixHSxMM = [[0 for x in range(len(strHS))] for x in range(len(strMM))] 
matrixHSxRM = [[0 for x in range(len(strHS))] for x in range(len(strRM))]
matrixHSxLE = [[0 for x in range(len(strHS))] for x in range(len(strLE))]
matrixHSxWA = [[0 for x in range(len(strHS))] for x in range(len(strWA))] 
numrowsHSvsMM = len(matrixHSxMM)  
numcolsHSvsMM = len(matrixHSxMM[0])
numrowsHSvsRM = len(matrixHSxRM)  
numcolsHSvsRM = len(matrixHSxRM[0]) 
numrowsHSvsLE = len(matrixHSxLE)  
numcolsHSvsLE = len(matrixHSxLE[0])
numrowsHSvsWA = len(matrixHSxWA)  
numcolsHSvsWA = len(matrixHSxWA[0]) 
print "Number of Rows"
print numcolsHSvsMM ,numrowsHSvsMM 
print numcolsHSvsRM ,numrowsHSvsRM
print numcolsHSvsLE ,numrowsHSvsLE 
print numcolsHSvsWA ,numrowsHSvsWA


#

print "Iterating Homo Sapiens And Mus Musculus"
aHSxMM =numpy.array(matrixHSxMM)
aHSxRM =numpy.array(matrixHSxRM)
aHSxLE =numpy.array(matrixHSxLE)
aHSxWA =numpy.array(matrixHSxWA)

#needleman-Wunsh Variables
gap = -4
match = 5
mismach = -3
alignmentA = ""
alignmentB = ""
similarityAlignment = 0.0

#needleman-Wunsh Functions
def initialization(aMatrix):
	for i in range(len(aMatrix)):
		aMatrix[i][0] = gap * i
	for j in range(len(aMatrix[0])):
		aMatrix[0][j] = gap * j 
	return aMatrix
def scoring(aMatrix,strA,strB):
	for i in range(len(aMatrix))[1:]:
		for j in range(len(aMatrix[0]))[1:]:
			if strA[j]==strB[i]:
				m = match
			else:
				m = mismach
			local_match = aMatrix[i-1][j-1] + m
			local_delet = aMatrix[i-1][j] + gap
			local_insert = aMatrix[i][-1] + gap
			aMatrix[i][j] = max (local_match,local_delet,local_insert)
	return aMatrix
def traceback(aMatrix,strA,strB):
	alA = ""
	alB = ""
	i = len(aMatrix)-1
	j = len(aMatrix[0])-1
	while(i>0 and j>0):
		score =	 aMatrix[i][j]
		scoreDiagonal = aMatrix[i-1][j-1]
		scoreAbove = aMatrix[i][j-1]
		scoreBeside = aMatrix[i-1][j]
		if strA[j]==strB[i]:
			m = match
		else:
			m = mismach
		if (score == scoreDiagonal + m):
			alA = strA[j] + alA
			alB = strB[i] + alB
			i-=1
			j-=1
		elif (Score == scoreBeside + d):
			alA = strA[j] + alA
			alB = "-" + alB
			j-=1
		elif (Score == scoreAbove + d):
			alA = "-" + alA
			alB = strB[i] + alB
			i-=1
		else:
			print "NOT GOOD!!!"
	while (i>0):
		alA = "-" + alA
		alB = strB[i] + alB
		i-=1
	while (j>0):
		alA = strA[j] + alA
		alB = "-" + alB
		j-=1

	return (alA, alB)
def similarity(alA, alB):
	lenAB = max(len(alA),len(alB))
	similarityValue = 0.0
	n =0	 
	c = 0
	while (c<lenAB):
		if(alA[c]==alB[c]):
			n+=1
		c+=1
	print "n ",n,lenAB
	similarityValue = ((n*1.0)/(lenAB*1.0))*100
	return similarityValue

numpy.set_printoptions(threshold='nan')
## 
print "Similarity between Homo Sapiens And Lemur"
aHSxLE = initialization(aHSxLE)
aHSxLE = scoring(aHSxLE, strHS,strLE)
print aHSxLE	
alignmentA, alignmentB = traceback(aHSxLE, strHS,strLE)
similarityAlignment = similarity(alignmentA,alignmentB)
print "AlignmentA is " , alignmentA 
print "AlignmentB is " , alignmentB
print "Similarity Alignment is " , similarityAlignment

#for (x,y), value in numpy.ndenumerate(aHSxMM):
#	if (strHS[y] == strMM[x]):
#		aHSxMM[x][y] = 1


print("--- %s seconds ---" % (time.time() - start_time))
