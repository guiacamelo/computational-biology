
import numpy
nos=0
def neighborJoin( matDist):
	global nos
	nos = nos + 1
	n = matDist.shape[0]

	matNJ = numpy.zeros((n,n), float)

	menor  = 1000
	menori = -1
	menorj = -1

	#Calcula matriz NJ
	for i in xrange(0,matNJ.shape[0]):
		for j in xrange(i+1,matNJ.shape[1]):
			matNJ[i][j] = (n-2)*matDist[i][j] - matDist[i].sum() - matDist[j].sum()

	for i in xrange(0,matNJ.shape[0]):
		for j in xrange(0,matNJ.shape[1]):
			if i == j:
				pass
			else:
				#Seleciona menor Neighbor
				if matNJ[i][j] < menor:
					menor  = matNJ[i][j]
					menori = i
					menorj = j
	#calcula Delta				
	delta = (matDist[menori].sum() - matDist[menorj].sum())/(n-2) 
	#Calcula distancias
	distanciai = 0.5*(matDist[menori][menorj]+delta)
	distanciaj = 0.5*(matDist[menori][menorj]-delta)
	print " i = " , menori,"   j = ",menorj
	print "Distancias i = " , distanciai,"   j = ",distanciaj
	newmatDist = matDist

	#Remove counas e linhas dos elementos com menor Neighbor. Remove primeiro o de maior indice para nao perder referencia dos indices
	if menori > menorj:
		newmatDist = numpy.delete(newmatDist,(menori),axis=0)
		newmatDist = numpy.delete(newmatDist,(menori),axis=1)
		newmatDist = numpy.delete(newmatDist,(menorj),axis=0)
		newmatDist = numpy.delete(newmatDist,(menorj),axis=1)
	else:
		newmatDist = numpy.delete(newmatDist,(menorj),axis=0)
		newmatDist = numpy.delete(newmatDist,(menorj),axis=1)
		newmatDist = numpy.delete(newmatDist,(menori),axis=0)
		newmatDist = numpy.delete(newmatDist,(menori),axis=1)
	sizei = newmatDist.shape[0]
	sizej = newmatDist.shape[1]
	#adiciona uma linha e uma coluna no inicio da nova matriz
	b = numpy.zeros((sizei+1,sizej+1))
	b[1:(b.shape[0]),1:(b.shape[0])] =  newmatDist
	newmatDist = b

	m = 0
	#calcula nova matriz
	for k in xrange(1,matDist.shape[0]):
		if k==m:
			pass
		else:
			newmatDist[k-1][m] = (matDist[menori][k] + matDist[menorj][k] - matDist[menori][menorj])/2
			newmatDist[m][k-1] = (matDist[menori][k] + matDist[menorj][k] - matDist[menori][menorj])/2
	print newmatDist
	if newmatDist.shape[0] <= 2:
		return newmatDist
	else:
		return neighborJoin(newmatDist)

n = 5 #4
matDist = numpy.zeros((n,n), float)


#Gorila
matDist[0][0] = 0
matDist[0][1] = 0.189
matDist[0][2] = 0.11
matDist[0][3] = 0.113
matDist[0][4] = 0.215

#Orangotango
matDist[1][0] = 0.189
matDist[1][1] = 0
matDist[1][2] = 0.179
matDist[1][3] = 0.192
matDist[1][4] = 0.211

#Humano
matDist[2][0] = 0.11
matDist[2][1] = 0.179
matDist[2][2] = 0
matDist[2][3] = 0.09405
matDist[2][4] = 0.2140

#chimpanze
matDist[3][0] = 0.113
matDist[3][1] = 0.192
matDist[3][2] = 0.094
matDist[3][3] = 0
matDist[3][4] = 0.214

#Gibao
matDist[4][0] = 0.215
matDist[4][1] = 0.211
matDist[4][2] = 0.205
matDist[4][3] = 0.214
matDist[4][4] = 0


# #Gorila
# matDist[0][0] = 0
# matDist[0][1] = 13
# matDist[0][2] = 21
# matDist[0][3] = 22

# #Orangotango
# matDist[1][0] = 13
# matDist[1][1] = 0
# matDist[1][2] = 12
# matDist[1][3] = 13

# #Humano
# matDist[2][0] = 21
# matDist[2][1] = 12
# matDist[2][2] = 0
# matDist[2][3] = 13

# #chimpanze
# matDist[3][0] = 22
# matDist[3][1] = 13
# matDist[3][2] = 13
# matDist[3][3] = 0
print matDist
neighborJoin(matDist)
print "Nos: ",nos