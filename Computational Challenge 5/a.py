
import time
import itertools
import numpy as np
n = 5000
l = 7
def Score(s):

	global n , l 
	motifs = list()
	ti=0
	for si in s:
		si = si + ti*n
		ti=ti+1
		motif = str[si:si+l]
		motifs.append(motif)
	print motifs
	
	matrix = [[0 for x in range(4)] for x in range(l)]
	for motif in motifs:
		for i in range(1,l-1,1):
			if motif[i] =='A':
				matrix[i][0] = matrix[i][0] +1
			if motif[i] =='C':
				matrix[i][1] = matrix[i][1] +1
			if motif[i] =='T':
				matrix[i][2] = matrix[i][2] +1
			if motif[i] =='G':
				matrix[i][3] = matrix[i][3] +1
	
	#MAX Score
	calcScore = 0
	
	consensus = [0 for x in range(l)]
	for i in range(4):
		maxim = 0
		for j in range(l):
			if matrix[j][i] > maxim:
				maxim = matrix[j][i]
				consensus[j]=motifs[i][j]
		calcScore = calcScore + maxim  
		maxim = 0
	return consensus, calcScore
start_time = time.time()
# sys.argv[0]
number_of_sequences = 0

fo = open("sequence.fasta", "r")
count = 0
header = fo.readline()
print header
str = fo.read();
fo.close()	
str = str.replace('\n', '').replace('\r', '')
sequenceSize = len(str)

m = 4
# calcula t
t = sequenceSize/n
d = [[] for x in xrange(t)]
for i in range(0,len(str),5000):
	#Divide sequentia em segmentos de 5000
	d.append(str[i:i+5000].split())

print len(d)

print len(d[0])
s = [[] for x in xrange(t)]
for ti in range(1,t,1):
	foo = list(xrange(5000))
	s.append(foo)
bestScore = 0
listOfS =  list(itertools.product(*s))
for s in listOfS:
	score = Score(s)
	if score > bestScore:
		bestScore =score
		bestMotif = s
s = list(xrange(t))
##print s
print Score(s)

# The Motif Finding Problem: Brute Force Solution
# >>> l=[ [ 1, 2, 3], [4], [5, 6] ]
# >>> list(itertools.product(*l))
# [(1, 4, 5), (1, 4, 6), (2, 4, 5), (2, 4, 6), (3, 4, 5), (3, 4, 6)]
# >>> l=[ [ 1, 2, 3], [4], [5, 6],[7,8] ]
# >>> list(itertools.product(*l))
# [(1, 4, 5, 7), (1, 4, 5, 8), (1, 4, 6, 7), (1, 4, 6, 8), (2, 4, 5, 7), (2, 4, 5, 8), (2, 4, 6, 7), (2, 4, 6, 8), (3, 4, 5, 7), (3, 4, 5, 8), (3, 4, 6,
#  7), (3, 4, 6, 8)]
# >>>
# for ni in range(1,n,1):
# 	s[0] = ni
# 	for ti in range(1,t,1):
# 		s[ti] = 0

#print str

print count

print("--- %s seconds ---" % (time.time() - start_time))
