## EXECUTION WITHOUT REPETITION
#set(['ACATA', 'ACAAA', 'ATAAA', 'AGAGA', 'ATAGA', 'AAACA', 'AAATA', 'AGACA', 'ATACA', 'ATATA', 'ACAGA', 'AAAGA', 'AGAAA', 'AAAAA', 'ACACA', 'AGATA'])
#set(['GAGGG', 'GTGGG', 'GTGAG', 'GCGCG', 'GCGAG', 'GGGCG', 'GTGTG', 'GAGTG', 'GTGCG', 'GAGAG', 'GCGTG', 'GGGAG', 'GGGTG', 'GAGCG', 'GCGGG', 'GGGGG'])
#set(['TGTTT', 'TTTTT', 'TGTAT', 'TTTCT', 'TCTGT', 'TATTT', 'TATAT', 'TATCT', 'TGTCT', 'TGTGT', 'TTTGT', 'TCTCT', 'TATGT', 'TCTTT', 'TTTAT', 'TCTAT'])
#set(['CCCAC', 'CCCGC', 'CGCAC', 'CACTC', 'CGCTC', 'CGCCC', 'CCCTC', 'CACCC', 'CTCAC', 'CGCGC', 'CACAC', 'CTCGC', 'CACGC', 'CCCCC', 'CTCCC', 'CTCTC'])
#64
#--- 30.2466027737 seconds ---


##EXECUTION with repetition
##8569635
##--- 72.3522088528 seconds ---

import time
import re
start_time = time.time()

number_of_sequences = 0

d = {}
# Open a file
fo = open("sequence.fasta", "r")
count = 0
header = fo.readline()
print header
str = fo.read();
fo.close()
str = str.replace('\n', '').replace('\r', '')


##Time for little instance: --- 0.00231695175171 seconds ---
##Time for big instance:--- 8.13726711273 seconds ---
subsequencesA = set(re.findall(r'([A][AGTC][A][AGTC][A])+',str))
subsequencesG = set(re.findall(r'([G][AGTC][G][AGTC][G])+',str))
subsequencesT = set(re.findall(r'([T][AGTC][T][AGTC][T])+',str))
subsequencesC = set(re.findall(r'([C][AGTC][C][AGTC][C])+',str))



##Time for little instance Time: --- 0.000761985778809 seconds ---
##Time for big instance Time: --- 14.9138979912 seconds ---
#subsequences = re.findall(r'([A][AGTC][A][AGTC][A])|([G][AGTC][G][AGTC][G])|([T][AGTC][T][AGTC][T])|([C][AGTC][C][AGTC][C])',str)

#print subsequences
#with open('littlepatternsequencewith set.fasta', 'w') as out_file:
#    out_file.write('\n'.join(subsequencesA +subsequencesG +subsequencesT +subsequencesC )) # This will create a string with all of the items in data separated by new-line characters
print subsequencesA 
print subsequencesG 
print subsequencesT 
print subsequencesC
print len(subsequencesA) + len(subsequencesG) +len(subsequencesT) +len(subsequencesC) 
print("--- %s seconds ---" % (time.time() - start_time))

